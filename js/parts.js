/*
Copyright 2021 J.Luis Álvarez.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
OF SUCH DAMAGE.
*/

'use strict';

const parts = {
    "Generic": [
        "blank",
        "joint-1234",
        "joint-123",
        "joint-124",
        "joint-134",
        "joint-234",
        "corner-12",
        "corner-14",
        "corner-23",
        "corner-34",
        "cross",
        "horizontal-conductor",
        "vertical-conductor",
		"lightbulb-13",
		"lightbulb-24",
		"push-13",
		"push-24",
		"switch-13",
		"switch-24",
		"commute-124",
		"commute-123",
		"commute-234",
		"commute-341",
		"terminal-1",
		"terminal-2",
		"terminal-3",
		"terminal-4",
		"corner-double-12a-32b",
		"conductor-double-2a4b-2b4a",
		"corner-double-14a-34b"
    ],
	"Power": [
		"battery-13",
		"battery-24",
		"rail-power-1",
		"rail-power-2",
		"rail-power-3",
		"rail-power-4",
		"rail-ground-1",
		"rail-ground-2",
		"rail-ground-3",
		"rail-ground-4",
		"earth-1"
	],
	"Passive": [
		"resistor-13",
		"resistor-24",
		"coil-13",
		"coil-24",
		"capacitor-13",
		"capacitor-24",
		"capacitor-pol-13",
		"capacitor-pol-24",
		"capacitor-pol-31",
		"capacitor-pol-42"
	],
	"Active": [
		"diode-13",
		"diode-24",
		"diode-31",
		"diode-42",
		"bjt-npn-413",
		"bjt-npn-213",
		"bjt-pnp-413",
		"bjt-pnp-213",
		"mosfet-n-231",
		"mosfet-n-431",
		"mosfet-p-231",
		"mosfet-p-431",
		"bjt-npn-431",
		"bjt-npn-231",
		"bjt-pnp-431",
		"bjt-pnp-231",
		"mosfet-n-213",
		"mosfet-n-413",
		"mosfet-p-213",
		"mosfet-p-413",
		"op-amp-4b4a",
		"op-amp-4a4b",
		"op-amp-rails-4b4a",
		"op-amp-rails-4a4b"
	],
	"Gates": [
		"gate-and",
		"gate-nand",
		"gate-or",
		"gate-nor",
		"gate-xor",
		"gate-xnor",
		"gate-buffer",
		"gate-not",
		"gate-and-ansi",
		"gate-nand-ansi",
		"gate-or-ansi",
		"gate-nor-ansi",
		"gate-xor-ansi",
		"gate-xnor-ansi",
		"gate-buffer-ansi",
		"gate-not-ansi"
	],
	"Other": [
		"led-arrows-vertical",
		"led-arrows-horizontal",
		"fuse-24",
		"fuse-13",
		"xtal-24",
		"xtal-13",
		"ic-pin-1",
		"ic-pin-inverted-1",
		"ic-pin-2",
		"ic-pin-inverted-2",
		"ic-pin-3",
		"ic-pin-inverted-3",
		"ic-pin-4",
		"ic-pin-inverted-4",
		"antenna"
	]
}

const img_dir = "svg";
const img_ext = ".svg";

const default_part = parts["Generic"][0];

//Convert a part name into an image path
function part_to_img(part) {
    //Find part id by searching on each category
    for (let category in parts) {
        for (let key in parts[category]) {
            if (parts[category][key] === part) {
                return img_dir + "/" + category.toLowerCase() + "/" + part + img_ext;
            }
        }
    }
    return img_dir + "/not-found" + img_ext;
}
