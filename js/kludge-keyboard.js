/*
Copyright 2021 J.Luis Álvarez.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
OF SUCH DAMAGE.
*/

'use strict';

// -------------------------------------
// --- Keyboard shortcuts for Kludge ---
// -------------------------------------


const key_mappings = {
    "KeyV": ["rail-power-3", "rail-power-4", "rail-power-1", "rail-power-2"],
    "KeyG": ["rail-ground-1", "rail-ground-2", "rail-ground-3", "rail-ground-4"],

    "KeyR": ["resistor-24", "resistor-13"],
    "KeyL": ["coil-24", "coil-13"],
    "KeyC": ["capacitor-24", "capacitor-13"],
    "KeyP": ["capacitor-pol-13", "capacitor-pol-24", "capacitor-pol-31", "capacitor-pol-42"],
    "KeyD": ["diode-13", "diode-24", "diode-31", "diode-42"],

    "Numpad1":        ["corner-12"],
    "Numpad2":        ["joint-124"],
    "Numpad3":        ["corner-14"],
    "Numpad4":        ["joint-123"],
    "Numpad5":        ["joint-1234"],
    "Numpad6":        ["joint-134"],
    "Numpad7":        ["corner-23"],
    "Numpad8":        ["joint-234"],
    "Numpad9":        ["corner-34"],
    "NumpadAdd":      ["cross"],
    "NumpadDivide":   ["vertical-conductor"],
    "NumpadMultiply": ["horizontal-conductor"],
    "NumpadDecimal":  ["blank"],
    "Numpad0":        ["horizontal-conductor"],
    "NumpadEnter":    ["vertical-conductor"]
};

// Key listener
document.onkeypress = function (event) {
    let code = String(event.code);
    if(code in key_mappings){
        console.log(`Key presed: ${event.code} -> ${String(key_mappings[code])}`);
        //Check if current part is in the mappings
        let current_part = String(get_current_part());
        if(key_mappings[code].includes(current_part)){
            //Find where the element is then set the next element
            let idx = key_mappings[code].indexOf(current_part) + 1;
            idx = idx % key_mappings[code].length;
            set_current_part(key_mappings[code][idx]);
        }
        //If not, just set the first element
        else{
            let mapping = key_mappings[code][0];
            set_current_part(mapping);
        }
    }
};
