/*
Copyright 2021 J.Luis Álvarez.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
OF SUCH DAMAGE.
*/

'use strict';

// ---------------------------
// --- Constants & Globals ---
// ---------------------------

//All SVG files are nominally 41px wide, for crisper edges they should
//be scaled up in integer multiples of this value.
const original_picture_size = 41;
const scaling_factor = 2;

//Set to true to enable debug comments like cell ids
const debug_export = false;

var current_part = default_part;
var picture_size = original_picture_size * scaling_factor;

// ------------------------
// --- Helper functions ---
// ------------------------

function make_part_img(part){
    let img = document.createElement('img');
    img.setAttribute("src", part_to_img(part));
    img.setAttribute("width", picture_size);
    img.setAttribute("height", picture_size);
    return img;
}

// ----------------------
// --- Parts selector ---
// ----------------------

// Fill-up the parts div with the components above
// Each <img> tag must have the class "ComponentGrid"
function populate_parts() {
    //Clear the "parts" element
    let parts_div = document.getElementById("parts");
    let iframes_div = document.getElementById("iframes_div");
    while (parts_div.firstChild) {
        parts_div.removeChild(parts_div.firstChild);
    }

    //For each part, create a <span> block with an <img> inside pointing to the
    //adequate .svg picture
    for (let category in parts) {
        let category_parts = parts[category];
		//Category button to show/hide parts
		let category_button = document.createElement("button");
		category_button.innerText = category;
		parts_div.appendChild(category_button);
		parts_div.appendChild(document.createElement("br"));
		//The parts themselves
        let category_div = document.createElement("div");
		category_div.setAttribute("id", `part_category_${category}`);
        parts_div.appendChild(category_div);
        for (let key in category_parts) {
            let part = category_parts[key];
            let part_span = document.createElement('span');
            let part_img = make_part_img(part);

            //Make the IMG element
            part_img.setAttribute("class", "ComponentGrid");
            part_img.setAttribute("id", part);

            //Connect the IMG element to a click listener
            part_img.addEventListener("click", function () { on_part_click(this.id); });

            part_span.appendChild(part_img);
            category_div.appendChild(part_span);

            //Add the same SVG as an <iframe> on the hidden "iframes_div"
            let part_iframe = document.createElement('iframe');
            part_iframe.setAttribute("id", "iframe_" + part);
            part_iframe.setAttribute("src", part_img.getAttribute("src"));
            iframes_div.appendChild(part_iframe);
        }
		
		category_button.onclick = function(){
			let category_div = document.getElementById(`part_category_${category}`);
			category_div.hidden = ! category_div.hidden;
		}
    }

    //Set default part as current part
    set_current_part(default_part);
}

function set_current_part(id) {
    console.log("Current part <" + id + ">");
    let part_box = document.getElementById("current_part");

    //Make the IMG element
    let part_img = make_part_img(id);
    part_img.setAttribute("class", "ComponentGrid");

    //Set global var
    current_part = id;

    part_box.appendChild(part_img);
    part_box.removeChild(part_box.firstChild);
}

function get_current_part() {
    return current_part;
}

function on_part_click(id) {
    set_current_part(id);
}

// -------------------
// --- Kludge grid ---
// -------------------

const grid_w = 20;
const grid_h = 20;

function populate_grid() {
    //Clear the "parts" <table> element
    let grid = document.getElementById("grid");
    while (grid.firstChild) {
        grid.removeChild(grid.firstChild);
    }

    //Make a grid_w by grid_h table
    for (let y = 0; y < grid_h; y++) {
        let row = document.createElement('tr');
        row.setAttribute("class", "KludgeGrid");
        for (let x = 0; x < grid_w; x++) {
            //Make Table Data
            let cell = document.createElement('td');
            cell.setAttribute("class", "KludgeGrid");
            //Make IMG attribute
            let cell_img = make_part_img(default_part);
            cell_img.setAttribute("id", "cell_" + x + "_" + y);
            //Connect listener
            cell_img.addEventListener("click", function () { on_cell_click(this.id); });

            cell.appendChild(cell_img);
            row.appendChild(cell);
        }
        grid.appendChild(row);
    }
}

function on_cell_click(id) {
    console.log("Setting cell <" + id + "> with part <" + current_part + ">");
    let cell_img = document.getElementById(id);
    cell_img.setAttribute("src", part_to_img(current_part));
}

// ---------------------
// --- Import-export ---
// ---------------------

function export_init(){
    let export_svg = document.getElementById("export_svg");
    export_svg.onclick = function() { on_export_svg_click(); };
    let export_txt = document.getElementById("export_txt");
    export_txt.onclick = function() { on_export_txt_click(); };
}

// --- Export as text ---
function on_export_txt_click(){
    console.log("[BEGIN] Exporting TXT");
    //Iterate over the grid 'img' elements, make an id -> picture
    let grid = document.getElementById("grid");
    const cells = grid.getElementsByTagName("img");
    let rv = {};
    for (let cell of cells) {
        let key = cell.getAttribute("id");
        //Get just the picture name, not the path
        rv[key] = cell.getAttribute("src").split("/").at(-1).split(".").at(0);
    }
    //Convert dictionary to json text
    let export_text = JSON.stringify(rv, null, 2);
    //Make a download link into the hidden_div and click it
    let hidden_div = document.getElementById("hidden_div");
    let download_a = document.createElement("a");
    download_a.setAttribute('href', 'data:text/plain;charset=utf-8,' + export_text);
    download_a.setAttribute('download', 'kludge-schematic-' + Math.floor(Date.now() / 1000) + ".txt");
    hidden_div.appendChild(download_a);
    download_a.click();
    hidden_div.removeChild(download_a);
    console.log("[ END ] Exporting TXT");
}

// --- Export as SVG ---
function on_export_svg_click(){
    let svg_head = `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
   width="${original_picture_size * grid_w * scaling_factor}"
   height="${original_picture_size * grid_h * scaling_factor}"
   version="1.1"
   id="svg5"
   shape-rendering="optimizeQuality"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Schematic export</title>
  <defs />
    `;
    let svg_tail = "\n</svg>";
    console.log("[BEGIN] Exporting SVG");
    //Iterate over the grid 'img' elements, make an id -> picture
    let grid = document.getElementById("grid");
    let x = new XMLSerializer();
    const cells = grid.getElementsByTagName("img");
    let rv = svg_head;
    //Add an outer group with scaling factor from scaling_factor
    rv += `<g transform="scale(${scaling_factor})">\n`;
    for (let cell of cells) {
        let key = cell.getAttribute("id");
        //Get just the picture name, not the path
        let part_id = cell.getAttribute("src").split("/").at(-1).split(".").at(0);

        if(part_id == "blank"){
            //If part_id is blank, just output a comment
            if(debug_export) rv +=  "\n<!-- [BLANK] CELL " + key + " -->\n"
        }
        else{
            //Build the SVG document contents
            if(debug_export) rv += "\n<!-- [BEGIN] CELL " + key + " -->\n";
            let part_iframe = document.getElementById("iframe_" + part_id);
            let part_source = part_iframe.contentDocument.getElementsByTagName("g")[0];
            let part_svg = x.serializeToString(part_source);
            //replace all ids with key+id
            part_svg = part_svg.replaceAll('id="', `id="${key}_`);

            //Create an outer <g> tag with a translate transform
            let x_translate=parseInt(key.split("_")[1]) * original_picture_size;
            let y_translate=parseInt(key.split("_")[2]) * original_picture_size;
            rv += `<g transform="translate(${x_translate} ${y_translate})">\n`;

            //Append the SVG drawing
            rv += part_svg;

            //Close the translation outer group with </g>
            rv += "\n</g>"

            if(debug_export) rv += "\n<!-- [ END ] CELL " + key + " -->\n";
        }
    }
    //Close the scaling <g>
    rv += "\n</g>";
    //Close document
    rv += svg_tail;
    let export_text = btoa(rv);
    //Make a download link into the hidden_div and click it
    let hidden_div = document.getElementById("hidden_div");
    let download_a = document.createElement("a");
    download_a.setAttribute('href', 'data:text/plain;base64,' + export_text);
    download_a.setAttribute('download', 'kludge-schematic-' + Math.floor(Date.now() / 1000) + ".svg");
    hidden_div.appendChild(download_a);
    download_a.click();
    hidden_div.removeChild(download_a);
    console.log("[ END ] Exporting SVG");
}

function import_init(){
    let import_btn = document.getElementById("import_button");
    import_btn.onclick = function() { on_import_click(); };
}

function on_import_click(){
    console.log("Importing schematic");
    //Get the textarea value and parse it as a json dict
    let import_textarea = document.getElementById("import_text");
    let import_text = import_textarea.value;
    let cells = JSON.parse(import_text);
    //Paint all the cells
    for(let key in cells){
        let cell = document.getElementById(key);
        cell.setAttribute("src", part_to_img(cells[key]))
    }
}
