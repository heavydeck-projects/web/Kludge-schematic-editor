#!/bin/sh

##########################################################################
# This script uses inkscape to convert SVG files with inkscape tags into #
# SVG files that do not have any inkscape-specific tags (plain SVG)      #
##########################################################################

echo "Cleaning-up SVG files"
#Prevent inkscape from opening a window (faster)
export DISPLAY=
cd svg-inkscape

TARGETS=''
echo '#Makefile for plain SVG exports' > Makefile.plain
for f in `find -iname '*.svg'`
do
  echo " -- Processing $f"
  DIRNAME=`dirname $f`
  FILENAME=`basename $f`
  OUT_FILE="../svg-plain/$DIRNAME/$FILENAME"
  mkdir -p "../svg-plain/$DIRNAME"
  echo "$OUT_FILE: $f" >> Makefile.plain
  echo -e '\tinkscape --export-overwrite --export-type=svg --export-plain-svg -o $@ $<' >> Makefile.plain
  TARGETS="$TARGETS $OUT_FILE"
done
echo "all: $TARGETS" >> Makefile.plain
make -j6 -f Makefile.plain all
rm Makefile.plain
cd ..
